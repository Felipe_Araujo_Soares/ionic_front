import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { App } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public email: String;
  public password: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public apiProvider: ApiProvider, public storage: Storage, public app: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  private getLoader() {
    return this.loadingCtrl.create({
      content: "Please wait..."
    });
  }

  login() {
    const loader = this.getLoader()

    loader.present();

    this.apiProvider.login(this.email.trim().toLowerCase(), this.password)
      .then(data => {
        let token = data.token
        this.storage.set('token', token)
        loader.dismiss()
        this.navCtrl.push(HomePage)
        this.app.getRootNav().setRoot(HomePage)
      }, err => {
        let msg = err.error ? err.error : 'Estamos com dificuldades para logar'
        alert(msg)
        loader.dismiss()
      })
  }

}
