import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  apiBackend = 'http://manosbtc.paxtecnologia.com.br:9000/api'

  constructor(public http: HttpClient) {}

  login(email, password):any {
    let user = { user: { email, password }}
    return new Promise((resolve, reject) => {
      this.http.post(this.apiBackend + '/authenticate', user).subscribe(data => {
        resolve(data);
      }, err => {
        reject(err)
      });
    });
  }

}
